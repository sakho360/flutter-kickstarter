# Flutter app template

This project is meant to be a starting point for a Flutter application that:

1. Has some basic features already implemented;
2. Follows the clean architecture model for flutter proposed by [Reso Coder](https://www.youtube.com/channel/UCSIvrn68cUk8CS8MbtBmBkA) in his [flutter clean architecture series](https://www.youtube.com/watch?v=dc3B_mMrZ-Q).

| Robert Martin's                                               | Reso Coder's                                                          |
| ------------------------------------------------------------- | --------------------------------------------------------------------- |
| ![clean architeture diagram](https://i.imgur.com/m2i1adH.jpg) | ![flutter clean architeture diagram](https://i.imgur.com/KhB6RzG.png) |

## Features

This app is already connfigured for:

- Automatically testing the whole codebase using gitlab's ci
- Linting with [lint](https://pub.dev/packages/lint)
- Localization (implemented from scratch as [here](https://www.youtube.com/watch?v=lDfbbTvq4qM))
- Navigation (using [sailor](https://pub.dev/packages/) as showed [here](https://www.youtube.com/watch?v=T1hzNcaAKiA))

Some additional packages are also included (equatable, mockito and dartz)

## .whats_this

There are some files called `.whats_this`. Those files are meant to be only temporary in order for git to include the empty folders but also as a quick reference for you to remind yourself what each folder is about. You can and most certainly should delete them in order to avoid unnecessary cluttering. All those files start with ".whats_this" on the first line so you can quickly find them even through text search.

## .vscode

This project also includes some vscode configuration files with personal preferences, deleting the .vscode folder will remove those customizations without affecting the rest of the project.

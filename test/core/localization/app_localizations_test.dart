import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/services.dart';
import 'package:myapp/core/localization/localizations_manager.dart';


void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  group("Each supported language is fully translated", () {
    Map<String, dynamic> primaryLocale;
    setUp(() async {
      final String first = supportedLocales.first.toLanguageTag();
      primaryLocale =
          jsonDecode(await rootBundle.loadString("lang/$first.json"))
              as Map<String, dynamic>;
    });
    for (final l in supportedLocales.sublist(1)) {
      test('${l.toLanguageTag()}', () async {
        //Act
        final Map<String, dynamic> m = jsonDecode(
                await rootBundle.loadString("lang/${l.toLanguageTag()}.json"))
            as Map<String, dynamic>;
        //Assert
        for (final s in primaryLocale.keys) {
          final String tmp = m.containsKey(s) ? s : null;
          expect(tmp, s);
        }
      });
    }
  });

  group("locales supported by the app have their file", () {
    for (final l in supportedLocales) {
      test('language ${l.toLanguageTag()} exists', () async {
        String s;
        s = await rootBundle.loadString("lang/${l.toLanguageTag()}.json");
        expect(s != null, true);
      });
    }
  });
}
